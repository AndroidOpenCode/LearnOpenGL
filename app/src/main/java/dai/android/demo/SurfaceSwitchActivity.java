package dai.android.demo;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import dai.android.opengl.R;
import dai.android.utility.log.Logger;

public class SurfaceSwitchActivity extends Activity {

    private static final String TAG = SurfaceSwitchActivity.class.getSimpleName();

    private static final int PICK_VIDEO_REQUEST = 1001;

    private MediaPlayer mMediaPlayer;
    private SurfaceHolder mHolderOne, mHolderTwo, mActiveHolder;

    private Uri mVideoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surface_switch);

        SurfaceView viewOne = findViewById(R.id.surfaceOne);
        viewOne.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                Logger.d(TAG, "surfaceView one callback: create");

                mHolderOne = surfaceHolder;
                if (null != mVideoUri) {
                    mMediaPlayer = MediaPlayer.create(getApplicationContext(), mVideoUri, mHolderOne);
                    mActiveHolder = mHolderOne;
                    mMediaPlayer.start();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                Logger.d(TAG, "Holder one destroy");
            }
        });

        SurfaceView viewTwo = findViewById(R.id.surfaceTwo);
        viewTwo.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                Logger.d(TAG, "surfaceView two callback: create");
                mHolderTwo = surfaceHolder;
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                Logger.d(TAG, "Holder two destroy");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_VIDEO_REQUEST && resultCode == RESULT_OK) {
            Logger.d(TAG, "got video: " + data.getData());
            mVideoUri = data.getData();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mVideoUri = null;
        if (null != mMediaPlayer) {
            mMediaPlayer.pause();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (null != mMediaPlayer) {
            mMediaPlayer.start();
        }
    }

    public void doSwitchSurface(View view) {
        if (null != mMediaPlayer && mMediaPlayer.isPlaying()) {
            mActiveHolder = mHolderOne == mActiveHolder ? mHolderTwo : mHolderOne;
            mMediaPlayer.setDisplay(mActiveHolder);
        }
    }

    public void doStartStop(View view) {
        if (null == mVideoUri) {
            Intent pickIntent = new Intent(Intent.ACTION_PICK);
            pickIntent.setTypeAndNormalize("video/*");
            startActivityForResult(pickIntent, PICK_VIDEO_REQUEST);
        } else {
            if (null != mMediaPlayer) {
                mMediaPlayer.stop();
                mMediaPlayer.release();
                mMediaPlayer = null;
            }
        }
    }
}
