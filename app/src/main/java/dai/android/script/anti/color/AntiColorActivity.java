package dai.android.script.anti.color;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.widget.ImageView;

import dai.android.opengl.R;

public class AntiColorActivity extends Activity {

    private Allocation mAllocIn, mAllocOut;
    private Bitmap mBitmapIn, mBitmapOut;
    private ScriptC_simple mScript;

    private ImageView mSrcImageView;
    private ImageView mDstImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anti_color);

        mSrcImageView = findViewById(R.id.src);
        mDstImageView = findViewById(R.id.dst);

        mBitmapIn = BitmapFactory.decodeResource(getResources(), R.drawable.anti_color_source);
        mBitmapOut = Bitmap.createBitmap(mBitmapIn.getWidth(), mBitmapIn.getHeight(), mBitmapIn.getConfig());
        mSrcImageView.setImageBitmap(mBitmapIn);

        RenderScript rs = RenderScript.create(this);
        mScript = new ScriptC_simple(rs);

        mAllocIn = Allocation.createFromBitmap(rs, mBitmapIn);
        mAllocOut = Allocation.createFromBitmap(rs, mBitmapIn);

        mScript.forEach_invert(mAllocIn, mAllocOut);
        mAllocOut.copyTo(mBitmapOut);

        mDstImageView.setImageBitmap(mBitmapOut);

        rs.destroy();
    }
}
