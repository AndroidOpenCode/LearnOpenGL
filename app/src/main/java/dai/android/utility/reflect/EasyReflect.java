package dai.android.utility.reflect;

import android.text.TextUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import dai.android.utility.log.Logger;

public final class EasyReflect {

    private static final String TAG = EasyReflect.class.getSimpleName();

    private EasyReflect() {
    }

    public static Object getInstance(String className, Object... params) {
        if (TextUtils.isEmpty(className)) {
            throw new IllegalArgumentException("class name must not null");
        }

        try {
            Class<?> clazz = Class.forName(className);

            Constructor constructor = null;
            if (null != params) {
                final int length = params.length;
                Class[] paramTypes = new Class[length];
                for (int i = 0; i < length; ++i) {
                    paramTypes[i] = params[i].getClass();
                }
                constructor = clazz.getDeclaredConstructor(paramTypes);
                constructor.setAccessible(true);

                return constructor.newInstance(params);
            }

            constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            return constructor.newInstance();

        } catch (ClassNotFoundException e1) {
            Logger.e(TAG, "class not found:", e1);
        } catch (NoSuchMethodException e2) {
            Logger.e(TAG, "no such method:", e2);
        } catch (IllegalAccessException e3) {
            Logger.e(TAG, "access exception:", e3);
        } catch (InstantiationException e4) {
            Logger.e(TAG, "instantiation exception:", e4);
        } catch (InvocationTargetException e5) {
            Logger.e(TAG, "invocation target exception:", e5);
        }

        return null;
    }

    public static Object invoke(String className, Object instance, String methodName, Object... params) {
        if (TextUtils.isEmpty(className)) {
            throw new IllegalArgumentException("class name must not null");
        }

        if (TextUtils.isEmpty(methodName)) {
            throw new IllegalArgumentException("method name must not null");
        }

        try {
            Class<?> clazz = Class.forName(className);
            if (null != params) {
                final int length = params.length;
                Class[] paramTypes = new Class[length];
                for (int i = 0; i < length; ++i) {
                    paramTypes[i] = params[i].getClass();
                }
                Method method = clazz.getDeclaredMethod(methodName, paramTypes);
                method.setAccessible(true);

                return method.invoke(instance, params);
            }

            Method method = clazz.getDeclaredMethod(methodName);
            method.setAccessible(true);
            return method.invoke(instance);

        } catch (ClassNotFoundException e1) {
            Logger.e(TAG, "class not found:", e1);
        } catch (NoSuchMethodException e2) {
            Logger.e(TAG, "no such method:", e2);
        } catch (IllegalAccessException e3) {
            Logger.e(TAG, "access exception:", e3);
        } catch (InvocationTargetException e4) {
            Logger.e(TAG, "invocation target exception:", e4);
        }

        return null;
    }

    public static Object invokeMethod(Object instance, Method method, Object... params) {
        if (null == method) {
            throw new IllegalArgumentException("method must not null");
        }
        method.setAccessible(true);

        try {
            return method.invoke(instance, params);
        } catch (IllegalAccessException e1) {
            Logger.e(TAG, "access exception:", e1);
        } catch (InvocationTargetException e2) {
            Logger.e(TAG, "invocation target exception:", e2);
        }

        return null;
    }

    public static Method getMethod(String className, String methodName, Class... paramTypes) {
        if (TextUtils.isEmpty(className)) {
            throw new IllegalArgumentException("class name must not null");
        }

        if (TextUtils.isEmpty(methodName)) {
            throw new IllegalArgumentException("method name must not null");
        }

        try {
            Class<?> clazz = Class.forName(className);
            if (null != paramTypes) {
                return clazz.getDeclaredMethod(methodName, paramTypes);
            }

            return clazz.getDeclaredMethod(methodName);

        } catch (ClassNotFoundException e1) {
            Logger.e(TAG, "class not found:", e1);
        } catch (NoSuchMethodException e2) {
            Logger.e(TAG, "no such method:", e2);
        }

        return null;
    }

    public static Field getField(String className, String fieldName) {
        if (TextUtils.isEmpty(className)) {
            throw new IllegalArgumentException("class name must not null");
        }

        if (TextUtils.isEmpty(fieldName)) {
            throw new IllegalArgumentException("field name must not null");
        }

        try {
            Class clazz = Class.forName(className);
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field;

        } catch (ClassNotFoundException e1) {
            Logger.e(TAG, "class not found:", e1);
        } catch (NoSuchFieldException e2) {
            Logger.e(TAG, "no such field:", e2);
        }

        return null;
    }

    public static Object getFieldValue(String className, Object instance, String fieldName) {
        Field field = getField(className, fieldName);
        if (null != field) {
            try {
                return field.get(instance);
            } catch (IllegalAccessException e) {
                Logger.e(TAG, "access exception:", e);
            }
        }

        return null;
    }

    public static void setFieldValue(String className, Object instance, String fieldName, Object value) {
        Field field = getField(className, fieldName);
        if (null != field) {
            try {
                field.set(instance, value);
            } catch (IllegalAccessException e) {
                Logger.e(TAG, "access exception:", e);
            }
        }
    }


}
