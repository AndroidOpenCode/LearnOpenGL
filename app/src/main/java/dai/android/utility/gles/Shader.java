package dai.android.utility.gles;

import android.content.res.Resources;
import android.opengl.GLES30;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import dai.android.utility.log.Logger;

public final class Shader {

    private static final String TAG = Shader.class.getSimpleName();

    public static int LoadShader(int shaderType, String source) {
        int shader = GLES30.glCreateShader(shaderType);
        if (0 != shader) {
            GLES30.glShaderSource(shader, source);
            GLES30.glCompileShader(shader);
            int[] compiled = new int[1];
            GLES30.glGetShaderiv(shader, GLES30.GL_COMPILE_STATUS, compiled, 0);
            if (0 == compiled[0]) {
                Logger.e(TAG, "Compile shader with type:" + shaderType + ":" +
                        GLES30.glGetShaderInfoLog(shader));
                GLES30.glDeleteShader(shader);
                shader = 0;
            }
        }
        return shader;
    }

    public static int CreateProject(String vSource, String fSource) {
        int vShader = LoadShader(GLES30.GL_VERTEX_SHADER, vSource);
        if (0 == vShader) {
            return 0;
        }

        int fShader = LoadShader(GLES30.GL_FRAGMENT_SHADER, fSource);
        if (0 == fShader) {
            return 0;
        }

        int project = GLES30.glCreateProgram();
        if (0 != project) {
            GLES30.glAttachShader(project, vShader);
            checkGLError("glAttachShader for vertex failed.");

            GLES30.glAttachShader(project, fShader);
            checkGLError("glAttachShader for fragment failed.");

            GLES30.glLinkProgram(project);

            int[] linkStatus = new int[1];
            GLES30.glGetProgramiv(project, GLES30.GL_LINK_STATUS, linkStatus, 0);

            if (GLES30.GL_TRUE != linkStatus[0]) {
                Logger.e(TAG, "can not link project: " + GLES30.glGetProgramInfoLog(project));
                GLES30.glDeleteShader(project);
                project = 0;
            }
        }

        return project;
    }

    public static void checkGLError(String operator) {
        int error;
        while ((error = GLES30.glGetError()) != GLES30.GL_NO_ERROR) {
            throw new RuntimeException("Operation info: " + operator + ", error number: " + error);
        }
    }

    public static String LoadFromAssetsFile(String fileName, Resources resources) {
        String value = null;
        try {
            InputStream inputStream = resources.getAssets().open(fileName);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            int ch = 0;
            while ((ch = inputStream.read()) != -1) {
                bout.write(ch);
            }

            byte[] buffer = bout.toByteArray();
            bout.close();
            inputStream.close();

            value = new String(buffer, "UTF-8");
            value = value.replaceAll("\\r\\n", "\n");

        } catch (IOException e) {
            Logger.e(TAG, "load asset file failed.", e);
        }
        return value;
    }


}
