
package dai.android.core.player;

import dai.android.player.misc.TimedText;

interface OnTimedTextListener {
    void onTimedText( in TimedText text );
}
