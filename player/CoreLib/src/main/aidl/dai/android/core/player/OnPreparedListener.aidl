
package dai.android.core.player;

interface OnPreparedListener {
    void onPrepared();
}
