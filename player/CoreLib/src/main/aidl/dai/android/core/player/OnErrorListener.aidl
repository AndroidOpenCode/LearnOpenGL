
package dai.android.core.player;

interface OnErrorListener {
    boolean onError( int what, int extra );
}
