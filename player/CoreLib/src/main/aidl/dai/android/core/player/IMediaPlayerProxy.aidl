
package dai.android.core.player;

import dai.android.core.process.Token;
import android.net.Uri;

interface IMediaPlayerProxy {

    void start( in Token token );
    void stop( in Token token );
    void pause( in Token token );
    void release( in Token token );
    void reset( in Token token );

    void setSurface( in Token token, in Surface surface );

    void seekTo( in Token token, in int msec );
    void seekTo2( in Token token, in long msec, in int mode );

    void setDataSource( in Token token, in String path );
    void setDataSource2( in Token token, in Uri uri );

    void prepare( in Token token );
    void prepareAsync( in Token token );

    boolean isPlaying();

    boolean isLooping();
    void setLooping( in Token token, boolean looping );

    int getDuration();
    int getCurrentPosition();

    int getVideoHeight();
    int getVideoWidth();

    void setScreenOnWhilePlaying( boolean screenOn );

}
