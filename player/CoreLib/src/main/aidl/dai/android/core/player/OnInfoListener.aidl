
package dai.android.core.player;

interface OnInfoListener {
    boolean onInfo( int what, int extra );
}