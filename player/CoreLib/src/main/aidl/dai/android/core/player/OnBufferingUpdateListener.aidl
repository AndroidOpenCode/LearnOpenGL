
package dai.android.core.player;

interface OnBufferingUpdateListener {
    void onBufferingUpdate( int percent );
}