
package dai.android.core.player;

interface OnSeekCompleteListener {
    void onSeekComplete();
}