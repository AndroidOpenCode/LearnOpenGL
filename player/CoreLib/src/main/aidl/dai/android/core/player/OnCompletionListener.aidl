
package dai.android.core.player;

interface OnCompletionListener {
    void onCompletion();
}