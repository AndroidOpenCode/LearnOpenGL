
package dai.android.core.player;

interface OnVideoSizeChangedListener {
    void onVideoSizeChanged( int width, int height );
}