package dai.android.core.process;

import android.app.ActivityManager;
import android.content.Context;
import android.support.annotation.NonNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;
import java.util.Locale;

public final class Process {

    public static int myPid() {
        return android.os.Process.myPid();
    }

    public static int myTid() {
        return android.os.Process.myTid();
    }

    public static int myUid() {
        return android.os.Process.myUid();
    }

    public static String getProcessName() {
        return getProcessName(myPid());
    }

    public static String getProcessName(int pid) {
        try {
            File file = new File(String.format(Locale.ENGLISH, "/proc/%d/cmdline", pid));
            BufferedReader mBufferedReader = new BufferedReader(new FileReader(file));
            String processName = mBufferedReader.readLine().trim();
            mBufferedReader.close();
            return processName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getProcessName(@NonNull Context context, int pid) {
        String processName = getProcessName(pid);
        if (null == processName) {
            if (null == context) {
                throw new IllegalArgumentException("argument context must not null");
            }

            ActivityManager activityManager =
                    (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            if (null == activityManager) {
                return null;
            }

            List<ActivityManager.RunningAppProcessInfo> runningApps =
                    activityManager.getRunningAppProcesses();
            if (runningApps == null) {
                return null;
            }

            for (ActivityManager.RunningAppProcessInfo processInfo : runningApps) {
                if (processInfo.pid == pid) {
                    return processInfo.processName;
                }
            }
        }
        return processName;
    }


}
