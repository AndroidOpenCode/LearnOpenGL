package dai.android.core.process;

import android.os.Parcel;
import android.os.Parcelable;

public final class Token implements Parcelable {
    private int mPid;
    private String mName;

    public Token(int pid, String processName) {
        mPid = pid;
        mName = processName;
    }

    public int getPid() {
        return mPid;
    }

    public String getName() {
        return mName;
    }

    protected Token(Parcel in) {
        mPid = in.readInt();
        mName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mPid);
        dest.writeString(mName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "{ " + mPid + " / " + mName + " }";
    }

    public static final Creator<Token> CREATOR = new Creator<Token>() {
        @Override
        public Token createFromParcel(Parcel in) {
            return new Token(in);
        }

        @Override
        public Token[] newArray(int size) {
            return new Token[size];
        }
    };

    public static Token makeToken() {
        final int pid = Process.myPid();
        final String name = Process.getProcessName();
        return new Token(pid, name);
    }
}
