package dai.android.player.local;

import android.annotation.TargetApi;
import android.media.MediaFormat;
import android.os.Build;

import dai.android.player.misc.IMediaFormat;

public class AndroidMediaFormat implements IMediaFormat {

    private final MediaFormat mMediaFormat;

    public AndroidMediaFormat(MediaFormat format) {
        mMediaFormat = format;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public String getString(String name) {
        if (null == mMediaFormat)
            return null;

        return mMediaFormat.getString(name);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int getInteger(String name) {
        if (null == mMediaFormat)
            return 0;

        return mMediaFormat.getInteger(name);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder(128);
        out.append(getClass().getName());
        out.append('{');
        if (mMediaFormat != null) {
            out.append(mMediaFormat.toString());
        } else {
            out.append("null");
        }
        out.append('}');
        return out.toString();
    }
}

