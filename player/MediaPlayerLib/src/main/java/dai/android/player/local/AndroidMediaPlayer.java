package dai.android.player.local;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaDataSource;
import android.media.MediaPlayer;
import android.media.TimedText;
import android.net.Uri;
import android.os.Build;
import android.view.Surface;
import android.view.SurfaceHolder;

import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Map;

import dai.android.player.AbstractMediaPlayer;
import dai.android.player.misc.IMediaDataSource;

public class AndroidMediaPlayer extends AbstractMediaPlayer {

    private static final String TAG = AndroidMediaPlayer.class.getSimpleName();

    private final MediaPlayer mInternalMediaPlayer;
    private final AndroidMediaPlayerListenerHolder mInternalListenerAdapter;

    public AndroidMediaPlayer() {
        synchronized (TAG) {
            mInternalMediaPlayer = new MediaPlayer();
        }
        mInternalMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mInternalListenerAdapter = new AndroidMediaPlayerListenerHolder(this);

        attachInternalListeners();
    }

    @Override
    public void setDisplay(SurfaceHolder surfaceHolder) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setDisplay(surfaceHolder);
        }
    }

    @Override
    public void setSurface(Surface surface) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setSurface(surface);
        }
    }

    @Override
    public void setDataSource(Context context, Uri uri) throws
            IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setDataSource(context, uri);
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void setDataSource(Context context, Uri uri, Map<String, String> headers) throws
            IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setDataSource(context, uri, headers);
        }
    }

    @Override
    public void setDataSource(FileDescriptor fd) throws
            IOException,
            IllegalArgumentException,
            IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setDataSource(fd);
        }
    }

    @Override
    public void setDataSource(FileDescriptor fd, long offset, long length) throws
            IOException,
            IllegalArgumentException,
            IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setDataSource(fd, offset, length);
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void setDataSource(AssetFileDescriptor afd) throws
            IOException,
            IllegalArgumentException,
            IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setDataSource(afd);
        }
    }

    @Override
    public void setDataSource(String path) throws
            IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setDataSource(path);
        }
    }

    private MediaDataSource mMediaDataSource;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void setDataSource(IMediaDataSource dataSource) throws
            IllegalArgumentException,
            IllegalStateException {
        if (null != mInternalMediaPlayer) {
            releaseMediaDataSource();
            mMediaDataSource = new MediaDataSourceProxy(dataSource);
            mInternalMediaPlayer.setDataSource(mMediaDataSource);
        }
    }

    @Override
    public void prepare() throws IOException, IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.prepare();
        }
    }

    @Override
    public void prepareAsync() throws IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.prepareAsync();
        }
    }

    @Override
    public void start() throws IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.start();
        }
    }

    @Override
    public void stop() throws IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.stop();
        }
    }

    @Override
    public void pause() throws IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.pause();
        }
    }

    @Override
    public boolean isPlaying() {
        return null != mInternalMediaPlayer && mInternalMediaPlayer.isPlaying();
    }

    @Override
    public void setLooping(boolean looping) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setLooping(looping);
        }
    }

    @Override
    public boolean isLooping() {
        return null != mInternalMediaPlayer && mInternalMediaPlayer.isLooping();
    }

    @Override
    public void release() {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.release();
        }
        releaseMediaDataSource();
        resetListeners();
        attachInternalListeners();
    }

    @Override
    public void reset() {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.reset();
        }
        releaseMediaDataSource();
        resetListeners();
        attachInternalListeners();
    }

    @Override
    public int getVideoHeight() {
        if (null != mInternalMediaPlayer) {
            return mInternalMediaPlayer.getVideoHeight();
        }
        return 0;
    }

    @Override
    public int getVideoWidth() {
        if (null != mInternalMediaPlayer) {
            return mInternalMediaPlayer.getVideoWidth();
        }
        return 0;
    }

    @Override
    public void setScreenOnWhilePlaying(boolean screenOn) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setScreenOnWhilePlaying(screenOn);
        }
    }

    @Override
    public void setVolume(float leftVolume, float rightVolume) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.setVolume(leftVolume, rightVolume);
        }
    }

    @Override
    public int getCurrentPosition() {
        if (null != mInternalMediaPlayer) {
            return mInternalMediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    @Override
    public int getDuration() {
        if (null != mInternalMediaPlayer) {
            return mInternalMediaPlayer.getDuration();
        }
        return 0;
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void seekTo(long msec, int mode) {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.seekTo(msec, mode);
        }
    }

    @Override
    public void seekTo(int msec) throws IllegalStateException {
        if (null != mInternalMediaPlayer) {
            mInternalMediaPlayer.seekTo(msec);
        }
    }

    @Override
    public String engineName() {
        return MediaPlayer.class.getSimpleName();
    }

    //
    //
    private void attachInternalListeners() {
        mInternalMediaPlayer.setOnPreparedListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnBufferingUpdateListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnCompletionListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnSeekCompleteListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnVideoSizeChangedListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnErrorListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnInfoListener(mInternalListenerAdapter);
        mInternalMediaPlayer.setOnTimedTextListener(mInternalListenerAdapter);
    }

    private void releaseMediaDataSource() {
        if (null != mMediaDataSource) {
            try {
                mMediaDataSource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mMediaDataSource = null;
        }
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    private class AndroidMediaPlayerListenerHolder implements
            MediaPlayer.OnPreparedListener,
            MediaPlayer.OnCompletionListener,
            MediaPlayer.OnBufferingUpdateListener,
            MediaPlayer.OnSeekCompleteListener,
            MediaPlayer.OnVideoSizeChangedListener,
            MediaPlayer.OnErrorListener,
            MediaPlayer.OnInfoListener,
            MediaPlayer.OnTimedTextListener {

        public final WeakReference<AndroidMediaPlayer> mWeakMediaPlayer;

        public AndroidMediaPlayerListenerHolder(AndroidMediaPlayer mp) {
            mWeakMediaPlayer = new WeakReference<>(mp);
        }

        @Override
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (self == null)
                return;
            notifyOnBufferingUpdate(percent);
        }

        @Override
        public void onCompletion(MediaPlayer mp) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (self == null)
                return;
            notifyOnCompletion();
        }

        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            return null != self && notifyOnError(what, extra);
        }

        @Override
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            return null != self && notifyOnInfo(what, extra);
        }

        @Override
        public void onPrepared(MediaPlayer mp) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (self == null)
                return;
            notifyOnPrepared();
        }

        @Override
        public void onSeekComplete(MediaPlayer mp) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (null == self)
                return;
            notifyOnSeekComplete();
        }

        @Override
        public void onTimedText(MediaPlayer mp, TimedText text) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (null == self) return;

            dai.android.player.misc.TimedText myText = null;
            if (null != text) {
                myText = new dai.android.player.misc.TimedText(text.getBounds(), text.getText());
            }
            notifyOnTimedText(myText);
        }

        @Override
        public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
            AndroidMediaPlayer self = mWeakMediaPlayer.get();
            if (null == self) return;

            notifyOnVideoSizeChanged(width, height);
        }
    }
}
