package dai.android.player.misc;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;

public final class TimedText implements Parcelable {

    private Rect mTextBounds = null;
    private String mTextChars = null;

    public TimedText() {
    }

    public TimedText(Rect bounds, String text) {
        mTextBounds = bounds;
        mTextChars = text;
    }

    protected TimedText(Parcel in) {
        mTextBounds = in.readParcelable(Rect.class.getClassLoader());
        mTextChars = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mTextBounds, flags);
        dest.writeString(mTextChars);
    }

    //public void readFromParcel(Parcel in) {
    //    mTextBounds = in.readParcelable(Rect.class.getClassLoader());
    //    mTextChars = in.readString();
    //}


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TimedText> CREATOR = new Creator<TimedText>() {
        @Override
        public TimedText createFromParcel(Parcel in) {
            return new TimedText(in);
        }

        @Override
        public TimedText[] newArray(int size) {
            return new TimedText[size];
        }
    };

    public Rect getBounds() {
        return mTextBounds;
    }

    public void setBounds(Rect bounds) {
        mTextBounds = bounds;
    }

    public void setText(String text) {
        mTextChars = text;
    }

    public String getText() {
        return mTextChars;
    }
}
