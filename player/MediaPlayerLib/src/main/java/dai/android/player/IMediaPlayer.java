package dai.android.player;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.Build;
import android.view.Surface;
import android.view.SurfaceHolder;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Map;

import dai.android.player.misc.IMediaDataSource;
import dai.android.player.misc.TimedText;

public interface IMediaPlayer {

    void setDisplay(SurfaceHolder surfaceHolder);

    void setSurface(Surface surface);

    void setDataSource(Context context, Uri uri) throws
            IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException;

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    void setDataSource(Context context, Uri uri, Map<String, String> headers) throws
            IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException;

    void setDataSource(FileDescriptor fd) throws
            IOException,
            IllegalArgumentException,
            IllegalStateException;

    void setDataSource(FileDescriptor fd, long offset, long length) throws
            IOException,
            IllegalArgumentException,
            IllegalStateException;

    @TargetApi(Build.VERSION_CODES.N)
    void setDataSource(AssetFileDescriptor afd) throws
            IOException,
            IllegalArgumentException,
            IllegalStateException;

    void setDataSource(String path) throws
            IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException;

    @TargetApi(Build.VERSION_CODES.M)
    void setDataSource(IMediaDataSource dataSource) throws
            IllegalArgumentException,
            IllegalStateException;

    void prepare() throws IOException, IllegalStateException;

    void prepareAsync() throws IllegalStateException;

    void start() throws IllegalStateException;

    void stop() throws IllegalStateException;

    void pause() throws IllegalStateException;

    boolean isPlaying();

    void setLooping(boolean looping);

    boolean isLooping();

    void release();

    void reset();

    int getVideoHeight();

    int getVideoWidth();

    void setScreenOnWhilePlaying(boolean screenOn);

    void setVolume(float leftVolume, float rightVolume);

    int getCurrentPosition();

    int getDuration();

    @TargetApi(Build.VERSION_CODES.O)
    void seekTo(long msec, int mode);

    void seekTo(int msec) throws IllegalStateException;

    String engineName();

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    void setOnPreparedListener(OnPreparedListener listener);

    void setOnCompletionListener(OnCompletionListener listener);

    void setOnBufferingUpdateListener(OnBufferingUpdateListener listener);

    void setOnSeekCompleteListener(OnSeekCompleteListener listener);

    void setOnVideoSizeChangedListener(OnVideoSizeChangedListener listener);

    void setOnErrorListener(OnErrorListener listener);

    void setOnInfoListener(OnInfoListener listener);

    void setOnTimedTextListener(OnTimedTextListener listener);


    //
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //

    interface OnPreparedListener {
        void onPrepared(IMediaPlayer mp);
    }

    interface OnCompletionListener {
        void onCompletion(IMediaPlayer mp);
    }

    interface OnBufferingUpdateListener {
        void onBufferingUpdate(IMediaPlayer mp, int percent);
    }

    interface OnSeekCompleteListener {
        void onSeekComplete(IMediaPlayer mp);
    }

    interface OnVideoSizeChangedListener {
        void onVideoSizeChanged(IMediaPlayer mp, int width, int height);
    }

    interface OnErrorListener {
        boolean onError(IMediaPlayer mp, int what, int extra);
    }

    interface OnInfoListener {
        boolean onInfo(IMediaPlayer mp, int what, int extra);
    }

    interface OnTimedTextListener {
        void onTimedText(IMediaPlayer mp, TimedText text);
    }

}
