package dai.android.player;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.view.Surface;
import android.view.SurfaceHolder;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Map;

import dai.android.player.misc.IMediaDataSource;
import dai.android.player.misc.TimedText;

public class MediaPlayerProxy implements IMediaPlayer {

    private static final String TAG = MediaPlayerProxy.class.getSimpleName();

    protected IMediaPlayer mCurrentPlayer;
    private OnSwitchEngineListener mSwitchListener;

    public MediaPlayerProxy(IMediaPlayer backup) {
        mCurrentPlayer = backup;
    }

    public IMediaPlayer getInternalMediaPlayer() {
        return mCurrentPlayer;
    }

    @Override
    public String engineName() {
        if (null != mCurrentPlayer) {
            return mCurrentPlayer.engineName();
        }
        return TAG;
    }

    public void setEngine(final IMediaPlayer newEngine) {
        if (null != mCurrentPlayer && null != newEngine) {
            // same engine, do nothing
            if (TextUtils.equals(mCurrentPlayer.engineName(), newEngine.engineName())) {
                return;
            }
            if (null != mSwitchListener) {
                IMediaPlayer old = MediaPlayerProxy.this;
                mSwitchListener.onSwitchEngine(old, newEngine);
            }
            mCurrentPlayer = newEngine;
        }
    }

    public void setOnSwitchEngineListener(OnSwitchEngineListener listener) {
        mSwitchListener = listener;
    }


    @Override
    public void setDisplay(SurfaceHolder surfaceHolder) {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setDisplay(surfaceHolder);
        }
    }

    @Override
    public void setSurface(Surface surface) {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setSurface(surface);
        }
    }

    @Override
    public void setDataSource(Context context, Uri uri) throws
            IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setDataSource(context, uri);
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void setDataSource(Context context, Uri uri, Map<String, String> headers) throws
            IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setDataSource(context, uri, headers);
        }
    }

    @Override
    public void setDataSource(FileDescriptor fd) throws
            IOException,
            IllegalArgumentException,
            IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setDataSource(fd);
        }
    }

    @Override
    public void setDataSource(FileDescriptor fd, long offset, long length) throws
            IOException,
            IllegalArgumentException,
            IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setDataSource(fd, offset, length);
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void setDataSource(AssetFileDescriptor afd) throws
            IOException,
            IllegalArgumentException,
            IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setDataSource(afd);
        }
    }

    @Override
    public void setDataSource(String path) throws
            IOException,
            IllegalArgumentException,
            SecurityException,
            IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setDataSource(path);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void setDataSource(IMediaDataSource dataSource) throws
            IllegalArgumentException,
            IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setDataSource(dataSource);
        }
    }

    @Override
    public void prepare() throws IOException, IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.prepare();
        }
    }

    @Override
    public void prepareAsync() throws IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.prepareAsync();
        }
    }

    @Override
    public void start() throws IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.start();
        }
    }

    @Override
    public void stop() throws IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.stop();
        }
    }

    @Override
    public void pause() throws IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.pause();
        }
    }

    @Override
    public boolean isPlaying() {
        return null != mCurrentPlayer && mCurrentPlayer.isPlaying();
    }

    @Override
    public void setLooping(boolean looping) {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setLooping(looping);
        }
    }

    @Override
    public boolean isLooping() {
        return null != mCurrentPlayer && mCurrentPlayer.isLooping();
    }

    @Override
    public void release() {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.release();
        }
    }

    @Override
    public void reset() {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.reset();
        }
    }

    @Override
    public int getVideoHeight() {
        if (null != mCurrentPlayer) {
            return mCurrentPlayer.getVideoHeight();
        }
        return 0;
    }

    @Override
    public int getVideoWidth() {
        if (null != mCurrentPlayer) {
            return mCurrentPlayer.getVideoWidth();
        }
        return 0;
    }

    @Override
    public void setScreenOnWhilePlaying(boolean screenOn) {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setScreenOnWhilePlaying(screenOn);
        }
    }

    @Override
    public void setVolume(float leftVolume, float rightVolume) {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.setVolume(leftVolume, rightVolume);
        }
    }

    @Override
    public int getCurrentPosition() {
        if (null != mCurrentPlayer) {
            return mCurrentPlayer.getCurrentPosition();
        }
        return 0;
    }

    @Override
    public int getDuration() {
        if (null != mCurrentPlayer) {
            return mCurrentPlayer.getDuration();
        }
        return 0;
    }

    @Override
    public void seekTo(long msec, int mode) {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.seekTo(msec, mode);
        }
    }

    @Override
    public void seekTo(int msec) throws IllegalStateException {
        if (null != mCurrentPlayer) {
            mCurrentPlayer.seekTo(msec);
        }
    }


    @Override
    public void setOnPreparedListener(OnPreparedListener listener) {
        if (null != mCurrentPlayer) {
            if (null != listener) {
                final OnPreparedListener finalListener = listener;
                mCurrentPlayer.setOnPreparedListener(new OnPreparedListener() {
                    @Override
                    public void onPrepared(IMediaPlayer mp) {
                        finalListener.onPrepared(MediaPlayerProxy.this);
                    }
                });

            } else {
                mCurrentPlayer.setOnPreparedListener(null);
            }
        }
    }

    @Override
    public void setOnCompletionListener(OnCompletionListener listener) {
        if (null != mCurrentPlayer) {
            if (null != listener) {
                final OnCompletionListener finalListener = listener;
                mCurrentPlayer.setOnCompletionListener(new OnCompletionListener() {
                    @Override
                    public void onCompletion(IMediaPlayer mp) {
                        finalListener.onCompletion(MediaPlayerProxy.this);
                    }
                });

            } else {
                mCurrentPlayer.setOnCompletionListener(null);
            }
        }
    }

    @Override
    public void setOnBufferingUpdateListener(OnBufferingUpdateListener listener) {
        if (null != mCurrentPlayer) {
            if (null != listener) {
                final OnBufferingUpdateListener finalListener = listener;
                mCurrentPlayer.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {
                    @Override
                    public void onBufferingUpdate(IMediaPlayer mp, int percent) {
                        finalListener.onBufferingUpdate(MediaPlayerProxy.this, percent);
                    }
                });
            } else {
                mCurrentPlayer.setOnBufferingUpdateListener(null);
            }
        }
    }

    @Override
    public void setOnSeekCompleteListener(OnSeekCompleteListener listener) {
        if (null != mCurrentPlayer) {
            if (null != listener) {
                final OnSeekCompleteListener finalListener = listener;
                mCurrentPlayer.setOnSeekCompleteListener(new OnSeekCompleteListener() {
                    @Override
                    public void onSeekComplete(IMediaPlayer mp) {
                        finalListener.onSeekComplete(MediaPlayerProxy.this);
                    }
                });
            } else {
                mCurrentPlayer.setOnSeekCompleteListener(null);
            }
        }
    }

    @Override
    public void setOnVideoSizeChangedListener(OnVideoSizeChangedListener listener) {
        if (null != mCurrentPlayer) {
            if (null != listener) {
                final OnVideoSizeChangedListener finalListener = listener;
                mCurrentPlayer.setOnVideoSizeChangedListener(new OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(IMediaPlayer mp, int width, int height) {
                        finalListener.onVideoSizeChanged(MediaPlayerProxy.this, width, height);
                    }
                });

            } else {
                mCurrentPlayer.setOnVideoSizeChangedListener(null);
            }
        }
    }

    @Override
    public void setOnErrorListener(OnErrorListener listener) {
        if (null != mCurrentPlayer) {
            if (null != listener) {
                final OnErrorListener finalListener = listener;
                mCurrentPlayer.setOnErrorListener(new OnErrorListener() {
                    @Override
                    public boolean onError(IMediaPlayer mp, int what, int extra) {
                        return finalListener.onError(MediaPlayerProxy.this, what, extra);
                    }
                });
            } else {
                mCurrentPlayer.setOnErrorListener(null);
            }
        }
    }

    @Override
    public void setOnInfoListener(OnInfoListener listener) {
        if (null != mCurrentPlayer) {
            if (null != listener) {
                final OnInfoListener finalListener = listener;
                mCurrentPlayer.setOnInfoListener(new OnInfoListener() {
                    @Override
                    public boolean onInfo(IMediaPlayer mp, int what, int extra) {
                        return finalListener.onInfo(MediaPlayerProxy.this, what, extra);
                    }
                });
            } else {
                mCurrentPlayer.setOnInfoListener(null);
            }
        }
    }

    @Override
    public void setOnTimedTextListener(OnTimedTextListener listener) {
        if (null != mCurrentPlayer) {
            if (null != listener) {
                final OnTimedTextListener finalListener = listener;
                mCurrentPlayer.setOnTimedTextListener(new OnTimedTextListener() {
                    @Override
                    public void onTimedText(IMediaPlayer mp, TimedText text) {
                        finalListener.onTimedText(MediaPlayerProxy.this, text);
                    }
                });
            } else {
                mCurrentPlayer.setOnTimedTextListener(null);
            }
        }
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public interface OnSwitchEngineListener {
        void onSwitchEngine(IMediaPlayer from, IMediaPlayer to);
    }


}
