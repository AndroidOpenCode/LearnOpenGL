package dai.android.play.service;

import android.os.Binder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.annotation.NonNull;

import java.util.Map;
import java.util.TreeMap;

import dai.android.core.log.Logger;

public final class CenterBinder extends Binder {
    private static final String TAG = CenterBinder.class.getSimpleName();

    private static final String DESCRIPTOR = "dai.android.play.service.binder.CenterInterface";

    private Binder mMainBinder;
    private Map<String, Binder> mBinders = new TreeMap<>();

    public void addService(Binder service) {
        addService(service, false);
    }

    public void addService(Binder service, boolean isMainService) {
        if (null == service) {
            throw new IllegalArgumentException("null argument in Binder");
        }

        final String desc = service.getInterfaceDescriptor();
        Logger.d(TAG, "add service: " + desc + (isMainService ? ", this main service" : ""));

        if (null == mMainBinder && isMainService) {
            mMainBinder = service;
        }

        mBinders.put(service.getInterfaceDescriptor(), service);
    }

    @Override
    public IInterface queryLocalInterface(@NonNull String descriptor) {
        return (IInterface) mBinders.get(descriptor);
    }

    @Override
    public String getInterfaceDescriptor() {
        return DESCRIPTOR;
    }

    @Override
    protected boolean onTransact(int code, @NonNull Parcel data, Parcel reply, int flags)
            throws RemoteException {
        Logger.d(TAG, "Center Binder onTransact");
        Logger.d(TAG, "code number = " + code);

        if (code >= FIRST_CALL_TRANSACTION && code <= LAST_CALL_TRANSACTION) {
            int pos = data.dataPosition();
            int strictPolicy = data.readInt();
            String descriptor = data.readString();
            data.setDataPosition(pos);

            Logger.d(TAG, "Not run at main binder, desc=" + descriptor);

            Binder service = mBinders.get(descriptor);
            if (service == null) {
                Logger.w(TAG, "Not found binder with descriptor=" + descriptor);

                data.enforceInterface(DESCRIPTOR);
                return false;
            } else {
                return service.transact(code, data, reply, flags);
            }
        }
        return mMainBinder.transact(code, data, reply, flags);
    }
}
